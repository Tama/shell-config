source projects.sh

cd /e/repos/*
for project in "${PROJECTS[@]}"; do
	cd ../$project
	echo "=========$project=========";
	echo "Checking out develop @ origin/develop..."
	git checkout -B develop origin/develop
	git submodule update --init --recursive
	git ls-files --others --exclude-standard | xargs -n 1 rm -rf
done