# Usage: pass the date of the last sprint demo to get a list of commit header messages, e.g.
# depmajorcommitssince.sh "2016-08-30 09:30:00"

source projects.sh

cd /e/repos/cig2unity/
for project in "${DEPMAJOR[@]}"; do
	cd ../$project
	echo "-----$project-----" >> ../commits.txt
	git log --pretty=oneline --abbrev-commit --after="$1" >> ../commits.txt
	echo "" >> ../commits.txt
	# to filter for more important changes, perhaps:
	# grep -i merge | grep -v release important_tcg.txt | grep -v "'develop' into" | grep -v "'origin' into" | grep -v "branch 'develop' of"
done