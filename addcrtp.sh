xargs sed 's/\([^ ]*\) : $1/\1 : $1<\1>/' -i

# Adds CRTP to C# classes. Pipe in a find command matching the files to modify. For example, find . -name "*Properties.cs" | xargs sed 's/\([^ ]*\) : BaseLevelProperties/\1 : BaseLevelProperties<\1>/' -i