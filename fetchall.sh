source projects.sh

cd /e/repos/cig2unity/
for project in "${PROJECTS[@]}"; do
	cd ../$project
	status=`git status --porcelain`
	git fetch --progress "origin"
done