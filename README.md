# Installation

Add the directories containing scripts you want to your PATH variable, then logout - login.

## Git config:

On Windows, use this as your C:\Users\YourName\.gitconfig file, assuming you checked out this repository into C:\users\YourName\source\:

    [include]
        path = source/shell-config/config/.gitconfig