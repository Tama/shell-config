
DIR='.'
if [ -z $1 ]; then
	DIR=$1
fi

find $DIR -name "*.cs" | xargs grep ^namespace | cut -d':' -f2 | sed 's/^namespace//' | cut -d'{' -f1 | tr -d ' ' | sort | uniq