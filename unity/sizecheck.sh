# Usage; sizecheck.sh Assets/Textures/Buildings Assets/objects.properties.txt
# Note; you must install ImageMagick and reboot before you can use this
# Gives false positives for textures in the given directory that do not match with buildings in the properties file

find "$1" -name "*.png" -or -name "*.jpg" | (while read filename; do 
	IMAGEWIDTH=`identify.exe -format "%[fx:w]" $filename`
	EXPECTEDTILES=`expr $IMAGEWIDTH / 100`
	JUSTFILE=`echo "$filename" | rev | cut -d'/' -f1 | rev | cut -d'.' -f1`
	XSIZE=`grep "^$JUSTFILE\.blocksX" $2 | cut -d'=' -f2 | tr -d ' '`;
	YSIZE=`grep "^$JUSTFILE\.blocksY" $2 | cut -d'=' -f2 | tr -d ' '`;
	#echo $IMAGEWIDTH;
	#echo $EXPECTEDTILES;
	#echo $JUSTFILE;
	#echo $XSIZE;
	#echo $YSIZE;
	#echo "==Debug info==";
	if [[ $XSIZE != $YSIZE ]]; then echo "$JUSTFILE is not square"; fi
	if [[ $XSIZE != $EXPECTEDTILES ]]; then echo "$JUSTFILE size is incorrect; Imagewidth $IMAGEWIDTH, tilesize $XSIZE"; fi
done)