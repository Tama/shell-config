#!/bin/bash
# Removes all added meta files that correspond to directories containing only added meta files
# This fixes the combination of git's ignorance of empty directories and Unity's desire to track them anyway
# Just like git clean, you must specify either -f or -n (-n is for a dry run, just printing what it would delete)
# For example:
# unity-emptyfolders.sh -n
# unity-emptyfolders.sh -f

# The empty line above is important, it distinguishes between the usage message (printed) and the code
# If no parameters, print help atop this shell file
if [ -z $1 ]; then
	sed -e '/^$/,$d' $0;
	exit 1;
fi

dryrun=1;
if [ ! -z $1 ]; then
	if [ $1 == "-f" ]; then
		dryrun=0;
	fi
fi

function allAddedMetas {
	{ git submodule foreach --recursive 'git ls-files --others --exclude-standard' | grep -v "^Entering\ " | grep ".meta$"; git ls-files --others --exclude-standard | grep ".meta$"; }
}

allAddedMetas > .addedmetas.txt

function otherwiseEmptyDirectories {
	# sed turns those into potential foldernames, those that are are checked for any contents other than exactly lines in .addedmetas.txt
	sed 's/.meta$//' .addedmetas.txt | (while read folder; do [ -d "$folder" ] && [ -z "`find "$folder" -type f | grep -v -Ff .addedmetas.txt`" ] && echo "$folder"; done)
}

otherwiseEmptyDirectories > .directoriestoremove.txt
sed 's/$/.meta/' .directoriestoremove.txt | grep -Ff .addedmetas.txt > .metastoremove.txt

if [ $dryrun -eq 1 ]; then
	echo "Dry run; Directories to be removed:"
	cat .directoriestoremove.txt
	echo "";
	echo "Dry run; Files to be removed:"
	cat .metastoremove.txt
	echo "";
else
	# -r because this is a directory removal; -f because the directories are not sorted and may be deleted in any order
	cat .directoriestoremove.txt | xargs -I {} rm -rf "{}"
	# -f because the files might already be gone because of the above
	cat .metastoremove.txt | xargs -I {} rm -f "{}"
fi

rm .addedmetas.txt
rm .directoriestoremove.txt
rm .metastoremove.txt