# Checks for the value set in a prefab file or scene file
# $1, parameter 1: The name of the serialized variable to find, for example androidKey for Store.cs - don't use the name in the editor, e.g. "Android Key"
# $2, parameter 2: The name of the prefab or scene with that script attached
# Example: printprefabvalue.sh androidKey Assets/Prefabs/GameState.prefab

# The empty line above is important, it distinguishes between the usage message (printed) and the code
# If no parameters, print help atop this shell file
if [ -z $1 ]; then
	sed -e '/^$/,$d' $0;
	exit 1;
fi

extension=`echo "$2" | cut -d'.' -f2`
if [[ $extension == "prefab" ]]; then
	printprefabvalue.sh "$1" "$2"
elif [[ $extension == "unity" ]]; then
	printscenevalue.sh "$1" "$2"
else 
	echo "unexpected filetype, should be either .prefab or .unity"
fi