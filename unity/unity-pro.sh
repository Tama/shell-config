# Revert all changes that change the licenseType to free
git diff -G "^licenseType" --diff-filter=M --name-only "*.meta" | xargs sed -i 's/licenseType: Free/licenseType: Pro/'