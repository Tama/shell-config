# Checks for the value set in a prefab file
# $1, parameter 1: The name of the serialized variable to find, for example androidKey for Store.cs - don't use the name in the editor, e.g. "Android Key"
# $2, parameter 2: The name of the prefab with that script attached
# Example: printprefabvalue.sh androidKey Assets/Prefabs/GameState.prefab

# The empty line above is important, it distinguishes between the usage message (printed) and the code
# If no parameters, print help atop this shell file
if [ -z $1 ]; then
	sed -e '/^$/,$d' $0;
	exit 1;
fi

grep "$1:" "$2" | cut -d':' -f2- | tr -d ' '