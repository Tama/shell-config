# Finds a C# file and returns the guid so you can then grep that in scenes and prefabs
# $1, parameter 1: The name of the C# class
# $2, parameter 2: (optional) The subfolder to search in; defaults to Assets/
# Example: printscenevalue.sh androidKey Assets/Scenes/Game.unity

# The empty line above is important, it distinguishes between the usage message (printed) and the code
# If no parameters, print help atop this shell file
if [ -z $1 ]; then
	sed -e '/^$/,$d' $0;
	exit 1;
fi

inputFolder='Assets'
if [ ! -z $2 ]; then
	# in all scripts, if a folder is passed with trailing /, remove it and continue
	inputFolder=`echo $2 | sed 's/\/$//'`;
fi

grep guid `find $inputFolder/ -name "$1.cs.meta"` | cut -d':' -f2 | tr -d ' '