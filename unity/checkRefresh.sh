source projects.sh

cd /e/repos/*
for project in "${PROJECTS[@]}"; do
	cd ../$project
	echo "=========$project=========";
	printvalue.sh refreshInterval Assets/Prefabs/GameState.prefab
	printvalue.sh refreshInterval Assets/Scenes/Game.unity
done