# Gets a value from prefabs
find Assets/Prefabs/ -type f -name "*.prefab" | xargs -I {} grep "$1" "{}" /dev/null