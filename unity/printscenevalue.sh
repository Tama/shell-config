# Checks for the value set in a scene file which might override a prefab's value
# $1, parameter 1: The name of the serialized variable to find, for example androidKey for Store.cs - don't use the name in the editor, e.g. "Android Key"
# $2, parameter 2: The name of the scene with that script attached
# Example: printscenevalue.sh androidKey Assets/Scenes/Game.unity

# The empty line above is important, it distinguishes between the usage message (printed) and the code
# If no parameters, print help atop this shell file
if [ -z $1 ]; then
	sed -e '/^$/,$d' $0;
	exit 1;
fi

# grep, show one line after as well, show the last line which is the next, print just the value
grep -A1 "propertyPath: $1" "$2" | tail -n 1 | cut -d':' -f2- | tr -d ' '