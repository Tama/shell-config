versionNumber=`echo $1 | cut -d'v' -f2 | cut -d'_' -f1`
sed -i "/Version/ s/\"[^\"]*\"/\"$versionNumber\"/" Assets/Scripts/GameVersion.cs
sed -i "/./ s/[^_]*/$versionNumber/" Assets/Version.txt