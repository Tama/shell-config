source projects.sh

cd /e/repos/*
for project in "${LOCALIZATION_PROJECTS[@]}"; do
	cd ../$project
	echo "";
	echo "=========$project=========";
	
	echo "Checking out develop @ origin/develop..."
	git checkout develop && git reset --hard origin/develop
	git submodule update --init --recursive
	git ls-files --others --exclude-standard | xargs -n 1 rm -rf
	
	echo "Checking localization status..."
	RESULT=`status.sh Assets/Resources usedkeys.txt | grep "No fatal errors encountered."`
	if [[ X"" = X"$RESULT" ]]; then
		echo "$project has non-clean localization status, skipping";
	else
		export.sh ../onesky/all_translations/ Assets/Resources/ usedkeys.txt
		RESULT=`status.sh Assets/Resources usedkeys.txt | grep "No fatal errors encountered."`
		if [[ X"" = X"$RESULT" ]]; then
			echo "After exporting, $project has non-clean localization status! Fix that!";
		else
			echo "Pushing new export commit";
			git commit -a -m "Re-export localizations"
			git push origin develop
		fi
	fi
done