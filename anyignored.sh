source projects.sh

cd /e/repos/*
for project in "${PROJECTS[@]}"; do
	cd ../$project
	echo "=========$project=========";
	grep "$1" .gitignore
done