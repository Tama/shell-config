source projects.sh

cd /e/repos/*
for project in "${PROJECTS[@]}"; do
	cd ../$project
	echo "=========$project=========";
	cat ProjectSettings.json
done