# Usage: `fuckflow.sh v1.3.2` to make a release with version number 1.3.2; `fuckflow.sh v1.3.2_2` to make a patch to that, with the same version number (but a new tag - only use when the previous was not released to SS)

# The empty line above is important, it distinguishes between the usage message (printed) and the code
# If no parameters, print help atop this shell file
if [ -z $1 ]; then
	sed -e '/^$/,$d' $0;
	exit 1;
fi

git fetch
git checkout -B master origin/master
git checkout -B develop origin/develop

git flow release start $1
version.sh $1
git add Assets/Scripts/GameVersion.cs Assets/version.txt
git commit -m "Bump to version $1"
git submodule update --init --recursive
git ls-files --others --exclude-standard | xargs -n 1 -I{} rm -rf "{}"
git flow release finish $1

git push origin --follow-tags
git checkout master
git push origin