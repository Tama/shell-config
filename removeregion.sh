xargs sed -i -e '/#region $1/,/#endregion $1/{ d; }'
# Pipe in the files to edit
# For example, see BEF commit b9271121f4f767; find . -name "*Properties.cs" | xargs sed -i -e '/#region Indexer/,/#endregion Indexer/{ d; }'