# Copies over all the images from a game repository
GAME=`echo $PWD | awk -F'/' '{print $NF}'`
RESKIN_FOLDER=`echo $GAME | sed 's/^/..\//' | sed 's/$/_reskin/'`
mkdir $RESKIN_FOLDER
find . -name "*.png" -o -name "*.jpg" | xargs -I{} cp --parents "{}" $RESKIN_FOLDER