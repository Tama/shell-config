find . -name "*.png" -or -name "*.jpg" | (while read filename; do echo $filename size=`identify.exe -format "%[fx:w]x%[fx:h]" "$filename"`; done)

# Prints image files and their sizes to the console.
# You need to install ImageMagick and reboot before you can use this script